# Airlock

Airlock is a Matrix bot that invites anybody who joins a space to also join its children. This is useful for getting people into your team or community's private rooms without having to send a bunch of separate invites by hand.

This is mostly a temporary solution until [knocking](https://github.com/matrix-org/synapse/pull/6739) and [restricted room membership](https://github.com/matrix-org/matrix-doc/pull/3083) make their way into a stable room version, which would make invites unecessary.

## Usage

1. Invite [@airlock:townsendandsmith.ml](https://matrix.to/#/@airlock:townsendandsmith.ml) to the space you want to manage, as well as all of the rooms and subspaces within it
2. Make sure the bot has permission to invite people to each of those rooms and subspaces

That's it! The bot will begin inviting anyone who joins the space to join everything inside it—no extra configuration is necessary.

## Running the bot yourself

First, register a bot user with your homeserver and [get an access token](https://t2bot.io/docs/access_tokens/).

```
npm install
cp config/default.yaml config/production.yaml
# Edit config/production.yaml as needed...
NODE_ENV=production npm run start
```

You'll almost certainly want to disable rate limiting for the bot, since it sends out lots of invites at once without any explicit rate limit handling. If you're using Synapse, you can configure this via the [Synapse admin APIs](https://github.com/matrix-org/synapse/blob/develop/docs/admin_api/user_admin_api.rst#set-ratelimit).
