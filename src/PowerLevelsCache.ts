import { MatrixClient, LogService } from "matrix-bot-sdk";

export default class PowerLevelsCache {
    private cache: Map<string, any> = new Map();

    constructor(private client: MatrixClient) {
        this.client.on("room.event", (roomId: string, event: any) => {
            try {
                if (event.type === "m.room.power_levels") {
                    this.cache.set(roomId, event.content);
                }
            } catch (err) {
                LogService.error("PowerLevelsCache", `Failed to handle event in ${roomId}: ${err}`);
            }
        });

        // Eagerly cache power levels on join
        this.client.on("room.join", roomId => this.get(roomId));
        // ...and delete them on leave
        this.client.on("room.leave", roomId => this.cache.delete(roomId));
    }

    public async get(roomId: string) {
        if (!this.cache.has(roomId)) {
            try {
                // TODO: Handle missing m.room.power_levels
                this.cache.set(roomId, await this.client.getRoomStateEvent(roomId, "m.room.power_levels", ""));
            } catch (err) {
                LogService.warn("PowerLevelsCache", `Failed to get power levels for ${roomId}: ${err}`);
            }
        }

        return this.cache.get(roomId);
    }
}
