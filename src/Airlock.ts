import { MatrixClient, LogService, Space } from "matrix-bot-sdk";
import PowerLevelsCache from "./PowerLevelsCache";
import JoinedMembersCache from "./JoinedMembersCache";

export default class Airlock {
    private userId: string;
    private joinedRooms: Set<string>;
    private powerLevelsCache: PowerLevelsCache;
    private joinedMembersCache: JoinedMembersCache;

    constructor(private client: MatrixClient) { }

    public async start() {
        this.userId = await this.client.getUserId();

        this.joinedRooms = new Set(await this.client.getJoinedRooms());
        this.client.on("room.join", roomId => {
            LogService.info("Airlock", `Joined ${roomId}`);
            this.joinedRooms.add(roomId);
        });
        this.client.on("room.leave", roomId => {
            LogService.info("Airlock", `Left ${roomId}`);
            this.joinedRooms.delete(roomId);
        });

        this.powerLevelsCache = new PowerLevelsCache(this.client);
        this.joinedMembersCache = new JoinedMembersCache(this.client, this.userId);

        this.client.on("room.event", async (roomId: string, event: any) => {
            try {
                await this.handleEvent(roomId, event);
            } catch (err) {
                LogService.error("Airlock", `Failed to handle event in ${roomId}: ${err}`);
            }
        });
    }

    private async handleEvent(roomId: string, event: any) {
        // Listen for other users joining spaces by invite
        if (event.type === "m.room.member" &&
            event.content?.membership === "join" &&
            event.unsigned?.prev_content?.membership === "invite" &&
            event.state_key !== this.userId) {
            let space;
            try {
                space = await this.client.getSpace(roomId);
            } catch (err) {
                // The join was to a normal room
                return;
            }

            await this.handleJoin(space, event.state_key, event.unsigned?.prev_sender);
        }
    }

    private async handleJoin(space: Space, userId: string, inviter: string) {
        LogService.info("Airlock", `Admitting ${userId} into ${space.roomId}...`);

        let children;
        try {
            children = Object.keys(await space.getChildEntities());
        } catch (err) {
            LogService.error("Airlock", `Couldn't get children for ${space.roomId}: ${err}`);
            return;
        }

        await Promise.all(children.map(async child => {
            try {
                // We only invite the user to a child room if:
                // 1. We're in the room
                // 2. We have invite permissions for the room
                // 3. The person who invited the user also has invite permissions for the room
                // 4. The user is not already in the room
                if (this.joinedRooms.has(child) &&
                    await this.canInviteTo(this.userId, child) &&
                    await this.canInviteTo(inviter, child) &&
                    !(await this.joinedMembersCache.get(child)).has(userId)) {
                    await this.client.inviteUser(userId, child);
                }
            } catch (err) {
                LogService.error("Airlock", `Failed to try inviting ${userId} to ${child}: ${err}`);
            }
        }));
    }

    private async canInviteTo(userId: string, roomId: string) {
        const joinedMembers = await this.joinedMembersCache.get(roomId);

        const powerLevels = await this.powerLevelsCache.get(roomId);

        let userPowerLevel = 0;
        if (powerLevels.users_default !== undefined) userPowerLevel = powerLevels.users_default;
        if (powerLevels.users?.[userId] !== undefined) userPowerLevel = powerLevels.users[userId];

        let invitePowerLevel = 50;
        if (powerLevels.invite !== undefined) invitePowerLevel = powerLevels.invite;

        return joinedMembers.has(userId) && userPowerLevel >= invitePowerLevel;
    }
}
