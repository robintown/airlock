import {
    AutojoinRoomsMixin,
    LogLevel,
    LogService,
    MatrixClient,
    PantalaimonClient,
    RichConsoleLogger,
    SimpleFsStorageProvider,
    SimpleRetryJoinStrategy,
} from "matrix-bot-sdk";
import * as path from "path";
import config from "./config";
import Airlock from "./Airlock";

LogService.setLogger(new RichConsoleLogger());
LogService.setLevel(LogLevel.INFO);

(async () => {
    // Prepare the storage system for the bot
    const storage = new SimpleFsStorageProvider(path.join(config.dataPath, "bot.json"));

    // Create the client
    let client: MatrixClient;
    if (config.pantalaimon.use) {
        const pantalaimon = new PantalaimonClient(config.homeserverUrl, storage);
        client = await pantalaimon.createClientWithCredentials(config.pantalaimon.username, config.pantalaimon.password);
    } else {
        client = new MatrixClient(config.homeserverUrl, config.accessToken, storage);
    }

    // Set up autojoin
    client.setJoinStrategy(new SimpleRetryJoinStrategy());
    AutojoinRoomsMixin.setupOnClient(client);

    LogService.info("index", "Starting bot...");
    const bot = new Airlock(client);
    try {
        await bot.start();
    } catch (err) {
        LogService.error(`Failed to start bot: ${err}`);
        process.exit(1);
    }

    LogService.info("index", "Starting sync...");
    await client.start(); // This blocks until the bot is killed
})();
