import * as config from "config";

interface IConfig {
    homeserverUrl: string;
    pantalaimon: {
        use: boolean;
        username: string;
        password: string;
    };
    accessToken: string;
    dataPath: string;
}

export default <IConfig>config;
