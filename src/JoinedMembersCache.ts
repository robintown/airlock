import { MatrixClient, LogService } from "matrix-bot-sdk";

export default class JoinedMembersCache {
    private cache: Map<string, Set<string>> = new Map();

    constructor(private client: MatrixClient, private userId: string) {
        this.client.on("room.event", async (roomId: string, event: any) => {
            try {
                await this.handleEvent(roomId, event);
            } catch (err) {
                LogService.error("JoinedMembersCache", `Failed to handle event in ${roomId}: ${err}`);
            }
        });

        // Eagerly cache joined members on join
        this.client.on("room.join", roomId => this.get(roomId));
        // ...and delete them on leave
        this.client.on("room.leave", roomId => this.cache.delete(roomId));
    }

    private async handleEvent(roomId: string, event: any) {
        if (event.type === "m.room.member" && event.state_key !== this.userId) {
            const newMembership = event.content?.membership;
            const oldMembership = event.unsigned?.prev_content?.membership;

            if (newMembership === "join" && oldMembership !== "join") {
                // The user joined
                (await this.get(roomId)).add(event.state_key);
            } else if (newMembership !== "join" && oldMembership === "join") {
                // The user left
                (await this.get(roomId)).delete(event.state_key);

            }
        }
    }

    public async get(roomId: string) {
        if (!this.cache.has(roomId)) {
            try {
                this.cache.set(roomId, new Set(await this.client.getJoinedRoomMembers(roomId)));
            } catch (err) {
                LogService.warn("JoinedMembersCache", `Failed to get joined members for ${roomId}: ${err}`);
            }
        }

        return this.cache.get(roomId);
    }
}
